<?php

namespace Database\Seeders;

use App\Models\Team;
use App\Models\Player;
use App\Models\Division;
use App\Models\Conference;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        //conferences
        Conference::create(['name' => 'Eastern Conference']);
        Conference::create(['name' => 'Western Conference']);

        //Divisions
        Division::create(['name' => 'Atlantic', 'conference_id' => 1]);
        Division::create(['name' => 'Central', 'conference_id' => 1]);
        Division::create(['name' => 'Southeast', 'conference_id' => 1]);
        Division::create(['name' => 'Northwest', 'conference_id' => 2]);
        Division::create(['name' => 'Pacific', 'conference_id' => 2]);
        Division::create(['name' => 'Southwest', 'conference_id' => 2]);

        $divisions = Division::all();
        $total_games = 82;

        foreach ($divisions as $d) {
            //create teams
            for ($i = 0; $i < 5; $i++) {
                $wins = $faker->numberBetween(0, 82);
                $losses = 82 - $wins;
                $team = Team::create([
                    'name' => $faker->state . ' ' . $faker->colorName,
                    'city' => $faker->city,
                    'division_id' => $d->id,
                    'wins' => $wins,
                    'losses' => $losses,
                    'coach_name' => $faker->lastName . ', ' . $faker->firstNameMale
                ]);

                //create 5 players per team
                $positions = ['C', 'PF', 'SF', 'SG', 'PG'];
                
                foreach ($positions as $p) {
                    Player::create([
                        'name' => $faker->lastName . ', ' . $faker->firstNameMale,
                        'team_id' => $team->id,
                        'position' => $p
                    ]);
                }

            }
        }

    }
}
