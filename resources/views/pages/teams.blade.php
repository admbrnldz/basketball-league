<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Basketball League</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

        <style>
            body {
                font-family: 'Nunito';
                background-color: white;
            }
        </style>
    </head>
    <body class="my-4">
        <div class="container-fluid">
            <h1>Basketball League</h1>
            <nav class="nav nav-pills nav-fill">
                <a class="nav-item nav-link {{ request()->group == 'conference' ? 'active' : '' }}" href="{{ route('teams', ['group' => 'conference']) }}">Conference</a>
                <a class="nav-item nav-link {{ request()->group == 'division' ? 'active' : '' }}" href="{{ route('teams', ['group' => 'division']) }}">Division</a>
            </nav>
            @foreach ($groups as $g)
            <div class="card my-4 shadow-sm">
                <div class="card-body">
                    <div class="card-title">
                        <h3>{{ $g['name'] }}</h3>
                    </div>
                    <table class="table table-sm table-striped">
                        <thead>
                            <tr>
                                <th>Rank</th>
                                <th>Team Name</th>
                                <th>City</th>
                                <th>Coach</th>
                                <th>Wins</th>
                                <th>Losses</th>
                                <th>Starting 5</th>
                            </tr>
                            
                        </thead>
                        <tbody>
                            @foreach ($g['teams'] as $t)
                            <tr>
                                @if (request()->group == 'conference')
                                <td>{{ $t->conference_rank }}</td>
                                @elseif(request()->group == 'division')
                                <td>{{ $t->division_rank }}</td>
                                @else
                                {{-- default to conference for now --}}
                                <td>{{ $t->conference_rank }}</td>
                                @endif
                                <td>{{ $t->name }}</td>
                                <td>{{ $t->city }}</td>
                                <td>{{ $t->coach_name }}</td>
                                <td>{{ $t->wins }}</td>
                                <td>{{ $t->losses }}</td>
                                <td>
                                    <ul>
                                        @foreach ($t->players as $p)
                                        <li>[{{ $p->position }}] {{ $p->name }}</li>
                                        @endforeach
                                    </ul>
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
            @endforeach
        </div>

        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    </body>
</html>
