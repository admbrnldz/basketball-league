<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Conference extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $with = ['division'];

    public function division()
    {
        return $this->hasMany(Division::class);
    }
}
