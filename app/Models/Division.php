<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $with = ['teams'];

    public function conference()
    {
        return $this->belongsTo(Conference::class);
    }

    public function teams()
    {
        return $this->hasMany(Team::class);
    }

}
