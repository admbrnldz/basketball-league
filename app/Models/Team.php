<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    protected $with = ['players'];

    public function players()
    {
        return $this->hasMany(Player::class);
    }

    public function division()
    {
        return $this->belongsTo(Division::class);
    }

    public function getDivisionRankAttribute()
    {
        //should be calculated using  game points. game behind etc.
        //get win ranking for now
        $teams = Team::where('division_id', $this->division_id)->orderBy('wins', 'desc')->get();
        //set wins as keys
        $rankings = array_keys($teams->groupBy('wins')->all());
        //get index of win (as key) +1 to determine win rank in division
        $rank = array_search($this->wins, $rankings);
        return $rank + 1;
    }

    public function getConferenceRankAttribute()
    {
         //should be calculated using  game points. game behind etc.
        //get win ranking for now
        $object = $this;
        $teams = Team::whereHas('division', function ($q) use ($object) {
            return $q->where('conference_id', $object->division->conference_id);
        })->orderBy('wins', 'desc')->get();
        //set wins as keys
        $rankings = array_keys($teams->groupBy('wins')->all());
        //get index of win (as key) +1 to determine win rank in conference
        $rank = array_search($this->wins, $rankings);
        return $rank + 1;
    }

}
