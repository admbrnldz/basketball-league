<?php

namespace App\Http\Controllers;

use App\Models\Team;
use Illuminate\Http\Request;

class TeamsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $teams = Team::orderBy('wins', 'desc')->get();
        $groupedBy = null;

        if ($request->get('group') == 'division') {
            $groupedBy = $teams->groupBy(function ($item, $key) {
                return $item->division->name;
            })->all();
        } else if ($request->get('group') == 'conference') {
            $groupedBy = $teams->groupBy(function ($item, $key) {
                return $item->division->conference->name;
            })->all();
        } else {
            $groupedBy = $teams->groupBy(function ($item, $key) {
                return $item->division->conference->name;
            })->all();
        }

        $groups = collect([]);

        foreach ($groupedBy as $k => $v) {
            $groups->push([
                'name' => $k,
                'teams' => $v
            ]);
        }

        return view('pages.teams', compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
